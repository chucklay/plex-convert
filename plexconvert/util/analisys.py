"""Path and File analysis tools"""

import asyncio
import os

MEDIA_EXTENSIONS = [".mp4", ".mkv", ".avi", ".wmv"]


async def get_media_file_list(base_path: str, file_list: list[str]=[], depth: int=-1, max_depth: int=-1, exclude_paths: list[str]=[]) -> list[str]:
    """Gets all media files at a provided path"""

    def _continue_recursion(check_dir: str):
        if max_depth >=0 and depth + 1 > max_depth:
            return False
        if os.path.join(base_path, check_dir) in exclude_paths:
            return False
        return True

    for _, directories, files in os.walk(base_path):
        for file in files:
            full_path = os.path.join(base_path, file)
            if os.path.splitext(full_path)[1].lower() in MEDIA_EXTENSIONS and os.path.exists(full_path):
                file_list.append(os.path.join(base_path, file))

        for directory in directories:
            if _continue_recursion(directory):
                await get_media_file_list(os.path.join(base_path, directory), file_list, exclude_paths)

    return file_list


async def get_media_info(file_path: str) -> dict[str, str]:
    """Gets info about the provided media at `file_path`"""
    raise NotImplementedError()
