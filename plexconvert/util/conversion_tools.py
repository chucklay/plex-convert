import logging
import os


LOGGER = logging.getLogger(__name__)
COPY_COMMAND = 'ffmpeg -i \"{ipath}\" -c copy \"{opath}\"'
CONVERT_COMMAND = 'ffmpeg -i \"{ipath}\" -c:v {encoder} h264_nvenc -preset slow -crf 20 -c:a aac +faststart \"{opath}\"'


async def convert_file(input_path: str, output_path: str, use_nvenc: bool=False, delete_original: bool=False, raise_exception: bool=False) -> str:
    """Converts a file to h.264/AAC mp4

    Args:
        input_path (str): The path to the file to convert
        output_path(str): The destination path to output the file to
        delete_original (bool): If true, deletes the file at `input_path` file on successful conversion,
                                otherwise leaves the source file as-is. Defaults to `False`

    Returns:
        str: the path to the output file. Should be identical to `output_path`
    """
    encoder = "libx264"
    if use_nvenc:
        encoder = "h264_nvenc"
    try:
        try:
            os.system('cmd /c "' + COPY_COMMAND.format(ipath=input_path, opath=output_path) + '"')
        except:
            os.system('cmd /c "' + CONVERT_COMMAND.format(ipath=input_path, encoder=encoder, opath=output_path) + '"')
    except:
        if raise_exception:
            raise
        LOGGER.exception(f"Unable to convert {input_path}")
    else:
        if delete_original:
            os.remove(input_path)
