import asyncio
import logging
import os
import click

from plexconvert.util.analisys import get_media_file_list
from plexconvert.util.conversion_tools import convert_file

LOGGER = logging.getLogger(__name__)

@click.command
@click.option("--entrypoint", required=True, type=str, help="The starting directory to convert")
@click.option("--nvenc", "-n", is_flag=True, help="Enable NVENC encoding (should be used on cards that support it)")
@click.option("--delete-original", "-d", is_flag=True, help="Delete original file on successful conversion")
async def main(entrypoint: str):
    """Application entrypoint"""

    file_list = await get_media_file_list(entrypoint)
    for fpath in file_list:
        fpath = fpath.replace("\\", "/")
        out_path = fpath.replace(os.path.splitext(fpath)[1], '.mp4')
        LOGGER.info(f"Converting: {fpath} -> {out_path}...")
        await convert_file(fpath, out_path)


if __name__ == "__main__":
    asyncio.run(main())
