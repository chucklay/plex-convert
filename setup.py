import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "Plex-Convert",
    version = "0.0.1",
    author = "Charlie Laymon",
    author_email = "chucklay@yahoo.com",
    description = ("A H.264/AAC mp4 media converter"),
    license = "MIT",
    url = "https://gitlab.com/chucklay/plex-convert",
    packages=['plexconvert'],
    long_description=read('README.md'),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: Microsoft :: Windows",
    ],
)
